<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザ新規登録</title>
</head>
<body>
<h3>ユーザ新規登録</h3>
<c:if test="${ not empty errMsgs }">
	<div class="errorMessages" style="color:#FF0000; font-size:10pt;">
		<c:forEach items="${errMsgs}" var="message">
			<c:out value="${message}" /><br/>
		</c:forEach>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
<form method="post">
	<table>
		<tr><td>氏名：</td><td><input name="name" type="text" value=""/></td></tr>
		<tr><td>アカウントID：</td><td><input name="account" type="text" value=""/></td></tr>
		<tr><td>パスワード：</td><td><input name="password" type="password"/></td></tr>
		<tr><td>支店</td><td>
			<select name="branch_id">
				<c:forEach items="${branchList}" var="branch">
					<option value="${branch.id}"><c:out value="${branch.name}"/></option>
				</c:forEach>
			</select>
		</td></tr>
		<tr><td>部署</td><td>
			<select name="position_id">
				<!--<c:forEach items="${positionList}" var="position">
					<option value="${position.id}"><c:out value="${position.name}"/></option>
				</c:forEach>-->
				<option value="1">営業部</option>
				<option value="2">技術部</option>
			</select>
		</td></tr>
		<tr><td>役職</td><td>
			<select name="position_id">
				<!--<c:forEach items="${positionList}" var="position">
					<option value="${position.id}"><c:out value="${position.name}"/></option>
				</c:forEach>-->
				<option value="1">マネジャー</option>
				<option value="2">社員</option>
			</select>
		</td></tr>
		<tr><td colspan="2"><input type="submit" value="登録"></td></tr>
	</table>
</form>

<a href="./">戻る</a>
</body>
</html>