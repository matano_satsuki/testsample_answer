package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exception.SQLRuntimeException;
import form.Branch;

public class BranchDao {
	/**
	 * 支店情報取得
	 * @param connection
	 * @return
	 */
	public List<Branch> getBranchList(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM branches";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<Branch> branchList = new ArrayList<>();
	        while(rs.next()) {
	        	Branch branch = new Branch();
	        	branch.setId(rs.getInt("id"));
	        	branch.setName(rs.getString("name"));
	        	branchList.add(branch);
	        }
	        if (branchList.isEmpty() == true) {
	            return null;
	        }
	        return branchList;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
}
