package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exception.SQLRuntimeException;
import form.User;

public class UserDao {
	/**
	 * ユーザ登録
	 * @param connection
	 * @param user
	 */
	public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	/**
	 * ユーザ情報取得
	 * @param connection
	 * @param accountOrEmail
	 * @param password
	 * @return
	 */
	public User getUser(Connection connection, String accountOrEmail,
	        String password) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE (account = ? OR email = ?) AND password = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, accountOrEmail);
	        ps.setString(2, accountOrEmail);
	        ps.setString(3, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	/**
	 * ユーザ情報確認
	 * @param connection
	 * @param account
	 * @return ユーザ数
	 */
	public int chkUser(Connection connection, String account) {

		int count = 0;
	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT COUNT(*) AS record_count FROM users WHERE account = ? ";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, account);

	        ResultSet rs = ps.executeQuery();
	        while(rs.next()) {
	        	count = rs.getInt("record_count");
        	}
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	    return count;
	}

	/**
	 * ユーザリスト生成
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String account = rs.getString("account");
	            String name = rs.getString("name");
	            String password = rs.getString("password");
	            int branch_id = rs.getInt("branch_id");
	            int position_id = rs.getInt("position_id");

	            User user = new User();
	            user.setId(id);
	            user.setAccount(account);
	            user.setName(name);
	            user.setPassword(password);
	            user.setBranch_id(branch_id);
	            user.setPosition_id(position_id);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}
}
