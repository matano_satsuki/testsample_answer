package test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import form.User;
import validator.UserValidator;

/**
 * UserValidatorテスト
 * @author matano.satsuki
 * @see validator.UserValidator
 *
 */
public class UserValidatorTest {

	// コンストラクタ（カバレッジ100にするため）
	UserValidator uservalidator = new UserValidator();

	/**
	 * 正常系
	 * 入力チェック
	 * 条件
	 *   全入力項目が入力範囲内
	 *   新規作成フラグtrue
	 * 期待値
	 *   エラーメッセージ件数：0件
	 */
	@Test
	public void allOK_001() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("NAME");
		user.setAccount("TESTACNT");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(0, is(resultMsgList.size()));
	}

	/**
	 * 正常系
	 * 入力チェック
	 * 条件
	 *   全入力項目が入力範囲内
	 *   新規作成フラグfalse
	 * 期待値
	 *   エラーメッセージ件数：0件
	 */
	@Test
	public void allOK_002() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("NAME");
		user.setAccount("TESTACNT");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, false);
		assertThat(0, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * アカウントID未入力
	 * 条件
	 *   名前のみ入力無し
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：アカウントIDを入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void accountNothingTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_ACCOUNT_NULL_OR_EMPTY, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * アカウントID不正
	 * 条件
	 *   アカウントIDに半角英数字以外が含まれる場合
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：アカウントIDは、半角英数字20文字以内で入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void accountErrTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ああああ");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_ACCOUNT, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * アカウントID文字数不正
	 * 条件
	 *   アカウントIDの文字数が不正
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：アカウントIDは、半角英数字20文字以内で入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void accountOverTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("123456789012345678901");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_ACCOUNT, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * 名前未入力
	 * 条件
	 *   名前のみ入力無し
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：名前を入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void nameNothingTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("");
		user.setAccount("TESTACNT");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_NAME_NULL_OR_EMPTY, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * 名前文字数不正
	 * 条件
	 *   名前の文字数が不正
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：名前は10文字以内で入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void nameOverTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("12345678901");
		user.setAccount("TESTACNT");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_NAME, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * パスワード未入力
	 * 条件
	 *   パスワード未入力
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：パスワードを入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void passwordNothingTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ACCOUNT");
		user.setPassword("");
		user.setChk_password("");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_PASSWORD_NULL_OR_EMPTY, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * パスワード不正
	 * 条件
	 *   パスワードに半角英数字以外
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：パスワードは、半角英数字8文字以上50文字以内で入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void passwordErrTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ACCOUNT");
		user.setPassword("TESTＰＷＤ123");
		user.setChk_password("TESTＰＷＤ123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_PASSWORD, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * パスワード不正
	 * 条件
	 *   パスワードが50文字以上
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：パスワードは、半角英数字8文字以上50文字以内で入力してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void passwordOverTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ACCOUNT");
		user.setPassword("12345678901234567890123456789012345678901234567890a");
		user.setChk_password("12345678901234567890123456789012345678901234567890a");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_PASSWORD, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * 正常異常系
	 * 確認用パスワード不正
	 * 条件
	 *   パスワードと確認用パスワードが不一致
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：パスワードとパスワード（確認用）には同じパスワードを設定してください。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void passwordUnMatchTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ACCOUNT");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123a");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_UNMATCH_PASSWORD, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常異常系
	 * 支店ID不正
	 * 条件
	 *   支店IDが範囲外
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：支店IDが不正です。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void branchErrTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ACCOUNT");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(0);
		user.setDivision_id(1);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_UNDIFINED_BRANCH, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常異常系
	 * 部署ID不正
	 * 条件
	 *   部署IDが範囲外
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：部署IDが不正です。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void divisionErrTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ACCOUNT");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(0);
		user.setPosition_id(1);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_UNDIFINED_DIVISION, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常異常系
	 * 役職ID不正
	 * 条件
	 *   役職IDが範囲外
	 *   新規作成フラグtrue
	 * 期待値
	 *   メッセージ内容(0)：役職IDが不正です。
	 *   エラーメッセージ件数：1件
	 */
	@Test
	public void positionhErrTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();
		user.setName("USERNAME");
		user.setAccount("ACCOUNT");
		user.setPassword("TESTPWD123");
		user.setChk_password("TESTPWD123");
		user.setBranch_id(1);
		user.setDivision_id(1);
		user.setPosition_id(0);

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(UserValidator.ERR_MSG_UNDIFINED_POSITION, is(resultMsgList.get(0)));
		assertThat(1, is(resultMsgList.size()));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常系
	 * 条件
	 *   支店IDが範囲内
	 * 期待値
	 *   返却値：false
	 */
	@Test
	public void branchChkTest001() {
		assertFalse(UserValidator.isUnDifinedBranch(1));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常異常系
	 * 条件
	 *   支店IDが範囲外
	 * 期待値
	 *   返却値：true
	 */
	@Test
	public void branchChkTest002() {
		assertTrue(UserValidator.isUnDifinedBranch(3));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常系
	 * 条件
	 *   部署IDが範囲内
	 * 期待値
	 *   返却値：false
	 */
	@Test
	public void divisionChkTest001() {
		assertFalse(UserValidator.isUnDifinedDivision(1));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常異常系
	 * 条件
	 *   部署IDが範囲外
	 * 期待値
	 *   返却値：true
	 */
	@Test
	public void divisionChkTest002() {
		assertTrue(UserValidator.isUnDifinedBranch(3));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常系
	 * 条件
	 *   役職IDが範囲内
	 * 期待値
	 *   返却値：false
	 */
	@Test
	public void positionChkTest001() {
		assertFalse(UserValidator.isUnDifinedPosition(1));
	}

	/**
	 * TODO: DB接続に切り替えた際はDBUnitによるテストが必要
	 * 正常異常系
	 * 条件
	 *   役職IDが範囲外
	 * 期待値
	 *   返却値：true
	 */
	@Test
	public void positionChkTest002() {
		assertTrue(UserValidator.isUnDifinedPosition(3));
	}
}
