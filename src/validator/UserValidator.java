package validator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import form.Branch;
import form.Division;
import form.Position;
import form.User;

/**
 * ユーザ情報入力チェッククラス
 * @author matano.satsuki
 *
 */
public class UserValidator {

	/****************************************
	 * メッセージ定義
	 ****************************************/
	/** エラーメッセージ：名前の未入力エラー */
	public final static String ERR_MSG_NAME_NULL_OR_EMPTY = "名前を入力してください。";
	/** エラーメッセージ：アカウントIDの未入力エラー */
	public final static String ERR_MSG_ACCOUNT_NULL_OR_EMPTY = "アカウントIDを入力してください。";
	/** エラーメッセージ：パスワードの未入力エラー */
	public final static String ERR_MSG_PASSWORD_NULL_OR_EMPTY = "パスワードを入力してください。";
	/** エラーメッセージ：アカウント名不正（英数字20文字以内ではない） */
	public static final String ERR_MSG_ACCOUNT = "アカウントIDは、半角英数字20文字以内で入力してください。";
	/** エラーメッセージ：パスワード不正（英数字8文字以上50文字以内ではない） */
	public static final String ERR_MSG_PASSWORD = "パスワードは、半角英数字8文字以上50文字以内で入力してください。";
	/** エラーメッセージ：名前の文字数不正 */
	public static final String ERR_MSG_NAME = "名前は10文字以内で入力してください。";
	/** エラーメッセージ：パスワードと確認用パスワードが不一致 */
	public static final String ERR_MSG_UNMATCH_PASSWORD = "パスワードとパスワード（確認用）には同じパスワードを設定してください。";
	/** エラーメッセージ：支店不正（範囲外のID） */
	public static final String ERR_MSG_UNDIFINED_BRANCH = "支店IDが不正です。";
	/** エラーメッセージ：部署不正（範囲外のID） */
	public static final String ERR_MSG_UNDIFINED_DIVISION = "部署IDが不正です。";
	/** エラーメッセージ：役職不正（範囲外のID） */
	public static final String ERR_MSG_UNDIFINED_POSITION = "役職IDが不正です。";

	/** 仮定義（本来はDBから取得する処理） */
	public static final List<Branch> branchMasterList = new ArrayList<>();
	public static final List<Division> divisionMasterList = new ArrayList<>();
	public static final List<Position> positionMasterList = new ArrayList<>();
	static {
		// 支店ダミー
		Branch bra1 = new Branch();
		bra1.setId(1);
		bra1.setName("ほげー");
		branchMasterList.add(bra1);
		Branch bra2 = new Branch();
		bra2.setId(2);
		bra2.setName("ふがー");
		branchMasterList.add(bra1);
		// 部署ダミー
		Division div1 = new Division();
		div1.setId(1);
		div1.setName("ふー");
		divisionMasterList.add(div1);
		Division div2 = new Division();
		div2.setId(2);
		div2.setName("ばー");
		divisionMasterList.add(div2);
		// 役職ダミー
		Position pos1 = new Position();
		pos1.setId(1);
		pos1.setName("にんー");
		positionMasterList.add(pos1);
		Position pos2 = new Position();
		pos2.setId(2);
		pos2.setName("ぴよー");
		positionMasterList.add(pos2);

	}

	/**
	 * リクエスト情報を基にユーザ情報のチェックを行います。
	 * @param リクエスト情報
	 * @param 新規作成フラグ true: 新規作成 false: 編集
	 * @return エラーメッセージリスト
	 */
	public static List<String> chkUserData(User user, boolean isSignUp) {
		List<String> errMsgs = new ArrayList<>();

		// 空チェック
		if (StringUtils.isEmpty(user.getName())) {
			errMsgs.add(ERR_MSG_NAME_NULL_OR_EMPTY);
		}
		if (StringUtils.isEmpty(user.getAccount())) {
			errMsgs.add(ERR_MSG_ACCOUNT_NULL_OR_EMPTY);
		}

		// アカウントチェック（英数字20文字以内ではない場合はエラー処理）
		if (StringUtils.isNotEmpty(user.getAccount()) &&
			!user.getAccount().matches("^[a-zA-Z0-9]{0,20}$")) {
			errMsgs.add(ERR_MSG_ACCOUNT);
		}

		// パスワード関連のチェックは新規登録時のみ実施
		if (isSignUp) {
			if(StringUtils.isEmpty(user.getPassword())) {
				errMsgs.add(ERR_MSG_PASSWORD_NULL_OR_EMPTY);
			} else {
				// パスワードチェック（英数字 かつ 8文字以上、50文字以内ではない場合エラー処理）
				if (!user.getPassword().matches("^[a-zA-Z0-9]{8,50}$")) {
					errMsgs.add(ERR_MSG_PASSWORD);
				}

				// 確認用パスワードチェック
				if (!user.getChk_password().equals(user.getPassword())) {
					errMsgs.add(ERR_MSG_UNMATCH_PASSWORD);
				}
			}
		}

		if (StringUtils.isNotEmpty(user.getName()) &&
			user.getName().length() > 10) {
			errMsgs.add(ERR_MSG_NAME);
		}

		// 支店が定義外の場合、エラー処理
		if (isUnDifinedBranch(user.getBranch_id())) {
			errMsgs.add(ERR_MSG_UNDIFINED_BRANCH);
		}
		if (isUnDifinedDivision(user.getDivision_id())) {
			errMsgs.add(ERR_MSG_UNDIFINED_DIVISION);
		}
		if (isUnDifinedPosition(user.getPosition_id())) {
			errMsgs.add(ERR_MSG_UNDIFINED_POSITION);
		}
		return errMsgs;

	}

	/**
	 * 支店IDの不正チェック
	 * @param branchID
	 * @return true:支店IDが定義外 false:支店IDが定義にある
	 */
	public static boolean isUnDifinedBranch(int branchID) {
		boolean result = true;
		for (Branch branch : branchMasterList) {
			if (branch.getId() == branchID) {
				result = false;
			}
		}
		return result;
	}

	/**
	 * 部署IDの不正チェック
	 * @param branchID
	 * @return true:部署IDが定義外 false:部署IDが定義にある
	 */
	public static boolean isUnDifinedDivision(int divisionID) {
		boolean result = true;
		for (Division division : divisionMasterList) {
			if (division.getId() == divisionID) {
				result = false;
			}
		}
		return result;
	}

	/**
	 * 役職IDの不正チェック
	 * @param branchID
	 * @return true:役職IDが定義外 false:役職IDが定義にある
	 */
	public static boolean isUnDifinedPosition(int positionID) {
		boolean result = true;
		for (Position position : positionMasterList) {
			if (position.getId() == positionID) {
				result = false;
			}
		}
		return result;
	}
}
